import { expect, jest } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import useEvent from "@testing-library/user-event";

import NewMessageForm from "./NewMessageForm";

describe("clicking the send button", () => {
    const sendHandler = jest.fn().mockName("sendHandler");

    const sendMessage = async () => {
        const user = useEvent.setup();

        render(<NewMessageForm onSend={sendHandler} />);

        await user.type(screen.getByTestId("messageText"), "New message");
        await user.click(screen.getByTestId("sendButton"));
    };

    it("clear the text field", async () => {
        await sendMessage();

        const expectedResult = screen.getByTestId<HTMLInputElement>("messageText").value;

        expect(expectedResult).toEqual("");
    });

    it("calls the send handler", async () => {
        await sendMessage();

        expect(sendHandler).toHaveBeenCalledWith("New message");
    });
});
