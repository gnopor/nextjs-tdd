import React, { useState } from "react";

interface IProps {
    onSend: (text: string) => void;
}

export default function NewMessageForm({ onSend }: IProps) {
    const [inputText, setInputText] = useState("");

    const handleTextChange = (text: string) => {
        setInputText(text);
    };

    const handleSendText = () => {
        onSend(inputText);

        setInputText("");
    };

    return (
        <form>
            <input
                type="text"
                data-testid="messageText"
                value={inputText}
                onChange={(e) => handleTextChange(e.target.value)}
            />

            <button type="button" data-testid="sendButton" onClick={() => handleSendText()}>
                Send
            </button>
        </form>
    );
}
