import React from "react";

interface IProps {
    data: string[];
}

export default function MessageList({ data }: IProps) {
    return (
        <div>
            <ul>
                {data.map((message, i) => (
                    <li key={i}>{message}</li>
                ))}
            </ul>
        </div>
    );
}
