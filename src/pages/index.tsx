import { useState } from "react";
import MessageList from "../components/MessageList";

import NewMessageForm from "../components/NewMessageForm";

export default function HomePage() {
    const [messages, setMessages] = useState<string[]>([]);

    const handleSend = (text: string) => {
        setMessages([text, ...messages]);
    };

    return (
        <main>
            <NewMessageForm onSend={handleSend} />
            <MessageList data={messages} />
        </main>
    );
}
